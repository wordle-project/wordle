import requests as rq
import random
DEBUG = False

class wordleBot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(wordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordleBot.creat_url, json=creat_dict)

        self.choices = [w for w in wordleBot.words]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(wordleBot.guess_url, json=guess)
            rj = response.json()
            feedback =  str(rj["feedback"])
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        for a in range(6):
	   if won:
	      print("Secret is", choice, "found in", len(tries), "attempts")
	      break
	   else:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
<<<<<<< HEAD
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        print("U lost")
=======
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
        print("Secret is", choice, "found in", len(tries), "attempts")
>>>>>>> 411d1b3eb0aa88f74df66584e62c0314112ad99a

    def update(self, choice: str, feedback: str):
        def common(choice: str, word: str):
            return 
        self.choices = [w for w in self.choices if common(choice, w)]


game = wordleBot("CodeShifu")
game.play()

